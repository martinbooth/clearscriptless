﻿namespace ClearScriptLess.Compiler.Stubs
{
    using Microsoft.ClearScript;

    public class Document
    {
        private readonly ScriptEngine engine;

        public Document(ScriptEngine engine)
        {
            this.engine = engine;
        }

        [ScriptMember("getElementsByTagName")]
        public object GetElementsByTagName(string tagName)
        {
            return this.engine.Evaluate("[]");
        }
    }
}
