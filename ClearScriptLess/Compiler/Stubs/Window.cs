﻿namespace ClearScriptLess.Compiler.Stubs
{
    using System.Dynamic;

    using Microsoft.ClearScript;

    public class Window : DynamicObject
    {
        private readonly ScriptEngine engine;

        public Window(ScriptEngine engine)
        {
            this.engine = engine;
            this.Location = new Location();
            this.Document = new Document(engine);
            //engine.Global["XMLHttpRequest"] = new XmlHttpRequest.XmlHttpRequestConstructor(engine);

            //engine.Global["alert"] = new JsFunction(engine, (@this, arguments) =>
            //    {
            //        MessageBox.Show(arguments[0].ToString());
            //        return Undefined.Value;
            //    });
        }

        [ScriptMember("document")]
        public Document Document { get; set; }

        [ScriptMember("location")]
        public Location Location { get; set; }

        public object Create(ScriptEngine engine)
        {
            dynamic obj = engine.Evaluate("new Object()");
            var windowJs = new Window(engine);
            obj.document = windowJs.Document;
            obj.location = windowJs.Location;

            obj.XMLHttpRequest = engine.Script.XMLHttpRequest;

            return obj;
        }
    }
}
