﻿namespace ClearScriptLess.Compiler.Stubs
{
    using Microsoft.ClearScript;

    public class Location
    {
        public Location()
        {
            this.HRef = string.Empty;
            this.Port = 0;
            this.Protocol = "file:";
            this.HostName = "localhost";
        }

        [ScriptMember("hostname")]
        public string HostName { get; set; }

        [ScriptMember("protocol")]
        public string Protocol { get; set; }

        [ScriptMember("port")]
        public int Port { get; set; }

        [ScriptMember("href")]
        public string HRef { get; set; }
    }
}
