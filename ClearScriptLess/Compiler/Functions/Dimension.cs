﻿namespace ClearScriptLess.Compiler.Functions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Microsoft.ClearScript;

    public class Dimension
    {
        private readonly string value;

        private readonly string unit;

        public Dimension(string value, string unit)
        {
            this.value = value;
            this.unit = unit;
        }

        internal dynamic CreateObjectInstance(ScriptEngine engine)
        {
            dynamic dimensionCreator = engine.Evaluate("(function(value, unit){return new window.less.tree.Dimension(value, unit);})");
            return dimensionCreator(this.value, this.unit);
        }
    }
}
