﻿namespace ClearScriptLess.Compiler.Functions
{
    public class FunctionParameter
    {
        public FunctionParameter(dynamic jsObject)
        {
            this.Value = jsObject["value"];
        }

        public object Value { get; set; }
    }
}