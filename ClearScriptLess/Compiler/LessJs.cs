﻿namespace ClearScriptLess.Compiler
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Web.Hosting;

    using ClearScriptLess.Compiler.Functions;
    using ClearScriptLess.Compiler.Stubs;
    using ClearScriptLess.Web.Configuration;

    using Microsoft.ClearScript;
    using Microsoft.ClearScript.V8;

    public class LessJs
    {
        private static ScriptEngine lessJsEngine;

        public LessJs()
        {
            this.Engine = new V8ScriptEngine();

            var windowJs = new Window(this.Engine);

            this.Engine.AddHostType("XMLHttpRequest", typeof(XmlHttpRequest));

            this.Engine.Script.window = windowJs.Create(this.Engine);

            this.Engine.AddHostObject("location", windowJs.Location);
            this.Engine.AddHostObject("document", windowJs.Document);
            this.Engine.Execute("window.less = {functions:{}}");

            this.AddCustomFunctions(this.Engine.Evaluate("window.less.functions"));

            this.Engine.Execute(File.ReadAllText(FindLessJs()));
        }

        internal static object CreateVariableArgumentJsFunction(Func<object, object> callback, ScriptEngine engine)
        {
            dynamic wrapFunctionCreator = engine.Evaluate("(function(x){return function(){return x(arguments)}})");

            return wrapFunctionCreator(callback);
        }

        public static ScriptEngine LessJsEngine
        {
            get
            {
                lessJsEngine = lessJsEngine ?? new LessJs().Engine;
                return lessJsEngine;
            }
        }

        private void AddCustomFunctions(dynamic functions)
        {
            var serviceConfigSection = (ClearScriptLessSection)ConfigurationManager.GetSection("clearscriptless");

            foreach (PluginElement plugin in serviceConfigSection.PluginElements)
            {
                var type = Type.GetType(plugin.Type);

                foreach (var method in type.GetMethods())
                {
                    var methodClosure = method;

                    var scriptMemberAttribute = (ScriptMemberAttribute)method.GetCustomAttributes(typeof(ScriptMemberAttribute), true).FirstOrDefault();

                    if (scriptMemberAttribute == null) 
                        continue;

                    functions[scriptMemberAttribute.Name.ToLower()] = CreateVariableArgumentJsFunction((dynamic input) =>
                        {
                            var arguments = from i in Enumerable.Range(0, (int)input.length) select new FunctionParameter(input[i]);
                            return ((Dimension)methodClosure.Invoke(null, arguments.ToArray())).CreateObjectInstance(this.Engine);
                        }, this.Engine);
                }
            }
        }

        public ScriptEngine Engine { get; private set; }

        private static string FindLessJs()
        {
            var scriptsFolder = HostingEnvironment.MapPath("~/Scripts");

            return Directory.GetFiles(scriptsFolder, "less-*.js")
                .First(f => !f.EndsWith(".min.js", StringComparison.OrdinalIgnoreCase));
        }
    }
}
