﻿namespace ClearScriptLess.Compiler
{
    using System.IO;


    using System;
    using System.Threading.Tasks;

    public class Parser
    {
        public Parser()
        {
        }

        public Task<Tree> ParseStringAsync(string input)
        {
            var parserJs = LessJs.LessJsEngine.Evaluate("new window.less.Parser()");

            return this.ParseAsync(input, parserJs);
        }

        public Task<Tree> ParseFileAsync(string fileName)
        {
            var uri = new Uri(fileName);
            var parserJs = ((dynamic)LessJs.LessJsEngine.Evaluate("(function(fn) { return new window.less.Parser({filename:fn})})"))(uri.AbsoluteUri);

            return this.ParseAsync(File.ReadAllText(fileName), parserJs);
        }

        private Task<Tree> ParseAsync(string input, dynamic parserJs)
        {
            return Task.Run(() =>
            {
                var t = new TaskCompletionSource<Tree>();

                Func<dynamic, object> parseComplete = (arguments) =>
                    {
                        var error = arguments[0];
                        if (error != null)
                        {
                            t.SetException(new LessParseException());
                        }
                        else 
                            t.SetResult(new Tree(arguments[1], parserJs));

                        return null;
                    };


                parserJs.parse(input, LessJs.CreateVariableArgumentJsFunction(parseComplete, LessJs.LessJsEngine));
                
                return t.Task;
            });
        }
    }
}
