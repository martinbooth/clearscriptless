﻿namespace ClearScriptLess.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;


    public class LessParseException : Exception

    {
        //public LessParseException(ObjectInstance jsError)
        //    : base(jsError["message"].ToString())
        //{
        //    this.LineNumber = Convert.ToInt32(jsError["line"]);
        //    this.FileName = (string)jsError["filename"];
        //    var extract = (ArrayInstance)jsError["extract"];
        //    this.Extract = string.Join("\r\n", from element in extract.ElementValues
        //                                       select element.ToString());
        //}

        public string Extract { get; set; }

        public string FileName { get; set; }

        public int LineNumber { get; set; }
    }
}
