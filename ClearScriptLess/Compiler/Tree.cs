﻿namespace ClearScriptLess.Compiler
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;

    public class Tree
    {
        private readonly dynamic jsTree;

        public Tree(dynamic jsTree, dynamic parserJs)
        {
            this.jsTree = jsTree;

            DynamicObject files = parserJs.imports.files;

            this.Dependencies = from f in files.GetDynamicMemberNames().ToList()
                                select new Uri(f).LocalPath;
        }

        public string ToCss()
        {
            return (string)jsTree.toCSS();
        }

        public IEnumerable<string> Dependencies { get; private set; }
    }
}
