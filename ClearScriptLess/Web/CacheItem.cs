﻿namespace ClearScriptLess.Web
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class CacheItem
    {
        public const int MaxSelectorCount = 4095;

        public CacheItem(string css, string fileName)
        {
            this.Css = css;
            this.CssParts = this.Parse(css);
            this.IeCss = this.CssParts != null ? this.GenerateParts(this.CssParts.Count, fileName) : css;
            this.FileSystemWatchers = new List<FileSystemWatcher>();
        }

        public CacheItem()
        {
            this.FileSystemWatchers = new List<FileSystemWatcher>();
        }

        public string Css { get; private set; }

        public List<string> CssParts { get; set; }

        public IList<FileSystemWatcher> FileSystemWatchers { get; private set; }

        public string IeCss { get; private set; }

        public List<string> Parse(string str)
        {
            var noComments = Regex.Replace(str, @"(\/\*[^*]*\*+([^/*][^*]*\*+)*\/)", string.Empty);

            var files = new List<string>();
            var rules = Regex.Matches(noComments, @"([^\{]+\{(?:[^\{\}]|\{[^\{\}]*\})*\})", RegexOptions.Multiline | RegexOptions.ECMAScript);

            var offset = 0;
            var selectorCount = 0;

            for (var i = 0; i < rules.Count; i++)
            {
                var matchArray = new List<string>();

                var regex = new Regex(@"(?:\s*@media\s*[^\{]*(\{))?(?:\s*(?:[^,\{]*(?:(,)|(\{)(?:[^\}]*\}))))");
                var rule = rules[i];

                var matches = regex.Matches(rule.Value);

                foreach (Match match in matches)
                {
                    for (var m = 1; m < match.Groups.Count; m++)
                    {
                        var group = match.Groups[m];
                        if (!string.IsNullOrEmpty(group.Value))
                        {
                            matchArray.Add(group.Value);
                        }
                    }
                }

                var matchCount = matchArray.Count;

                if (selectorCount + matchCount > MaxSelectorCount)
                {
                    AddFile(rules, offset, i, files);

                    offset = i;
                    selectorCount = 0;
                }

                selectorCount += matchCount;
            }

            if (files.Count == 0)
                return null;

            AddFile(rules, offset, rules.Count, files);

            return files;
        }

        private static void AddFile(MatchCollection rules, int offset, int i, List<string> files)
        {
            var slice = new List<string>(rules.OfType<Match>().Skip(offset).Take(i - offset).Select(r => r.Value));

            if (slice.Count > 0)
            {
                slice[0] = Regex.Replace(slice[0], @"^\s+", string.Empty);

                files.Add(string.Join(string.Empty, slice));
            }
        }

        private string GenerateParts(int count, string fileName)
        {
            return string.Join(
                "\n",
                Enumerable.Range(0, count)
                    .Select(n => "@import url(\'" + fileName + "?part=" + n + "\');"));
        }
    }
}
