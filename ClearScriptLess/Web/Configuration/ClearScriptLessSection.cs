﻿namespace ClearScriptLess.Web.Configuration
{
    using System.Configuration;

    public class ClearScriptLessSection : ConfigurationSection
    {
        [ConfigurationProperty("plugins", IsRequired = true)]
        public PluginElementsCollection PluginElements
        {
            get
            {
                return (PluginElementsCollection)base["plugins"];
            }
        }
    }
}
