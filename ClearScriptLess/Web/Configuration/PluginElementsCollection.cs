﻿namespace ClearScriptLess.Web.Configuration
{
    using System.Configuration;

    [ConfigurationCollection(typeof(PluginElement), AddItemName = "plugin")]
    public class PluginElementsCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new PluginElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var pluginElement = (PluginElement)element;
            return pluginElement;
        }
    }
}