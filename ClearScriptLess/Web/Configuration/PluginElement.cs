﻿namespace ClearScriptLess.Web.Configuration
{
    using System.Configuration;

    public class PluginElement : ConfigurationElement
    {
        [ConfigurationProperty("type", IsKey = true, IsRequired = true)]
        public string Type
        {
            get
            {
                return base["type"] as string;
            }

            set
            {
                base["type"] = value;
            }
        }
    }
}