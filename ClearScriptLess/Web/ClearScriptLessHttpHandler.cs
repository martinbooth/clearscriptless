﻿namespace ClearScriptLess.Web
{
    using System.Threading.Tasks;
    using System.Web;

    using ClearScriptLess.Compiler;

    public class ClearScriptLessHttpHandler : HttpTaskAsyncHandler
    {
        public override bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public override async Task ProcessRequestAsync(HttpContext context)
        {
            var path = context.Request.Url.AbsolutePath;

            var physicalPath = context.Request.MapPath(path);

            int? part = null;
            var showIeCss = false;

            if (context.Request.QueryString["part"] != null)
                part = int.Parse(context.Request.QueryString["part"]);
            else if (context.Request.QueryString["ie"] != null)
                showIeCss = true;

            var less = Cache.Current[physicalPath];

            if (less == null)
            {
                Tree tree;
                string css;
                try
                {
                    tree = await new Parser().ParseFileAsync(physicalPath);
                    css = tree.ToCss();
                }
                catch (LessParseException ex)
                {
                    this.WriteException(context, ex);
                    return;
                }

                less = Cache.Current.AddOrUpdate(physicalPath, tree.Dependencies, css);
            }

            context.Response.ContentType = "text/css";

            context.Response.Write(part != null ? less.CssParts[part.Value] : showIeCss ? less.IeCss : less.Css);
        }

        private void WriteException(HttpContext context, LessParseException ex)
        {
            context.Response.ContentType = "text/plain";
            context.Response.StatusCode = 500;
            context.Response.Write("Error occured parsing file: " + ex.FileName + " on line " + ex.LineNumber + "\r\n");
            context.Response.Write(ex.Message + "\r\n");
            context.Response.Write("near: <");
            context.Response.Write(ex.Extract);
            context.Response.Write(">");
        }
    }
}