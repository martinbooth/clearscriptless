﻿namespace ClearScriptLess.Web
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class Cache
    {
        private static readonly Cache current = new Cache();

        private readonly ConcurrentDictionary<string, CacheItem> store;

        public Cache()
        {
            this.store = new ConcurrentDictionary<string, CacheItem>(StringComparer.OrdinalIgnoreCase);
        }

        public static Cache Current
        {
            get
            {
                return current;
            }
        }

        public CacheItem this[string key]
        {
            get
            {
                CacheItem value;
                this.store.TryGetValue(key, out value);
                return value;
            }
        }

        public CacheItem AddOrUpdate(string path, IEnumerable<string> dependencies, string fileContents)
        {
            var cacheItem = new CacheItem(fileContents, Path.GetFileName(path));

            this.store.AddOrUpdate(path, cacheItem, (k, ov) => cacheItem);

            FileSystemEventHandler removeFileFromCache = (sender, e) =>
            {
                CacheItem ignore;
                this.store.TryRemove(path, out ignore);

                foreach (var fsw in cacheItem.FileSystemWatchers)
                    fsw.Dispose();
            };

            foreach (var file in new[] { path }.Concat(dependencies))
                cacheItem.FileSystemWatchers.Add(this.CreateFileSystemWatcher(file, removeFileFromCache));

            return cacheItem;
        }

        private FileSystemWatcher CreateFileSystemWatcher(string path, FileSystemEventHandler fileChangedDelegate)
        {
            var fsw = new FileSystemWatcher
            {
                Path = Path.GetDirectoryName(path),
                Filter = Path.GetFileName(path),
                EnableRaisingEvents = true,
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName
            };

            fsw.Changed += fileChangedDelegate;
            fsw.Created += fileChangedDelegate;
            fsw.Deleted += fileChangedDelegate;
            fsw.Renamed += (sender, e) => fileChangedDelegate(sender, e);

            return fsw;
        }
    }
}
