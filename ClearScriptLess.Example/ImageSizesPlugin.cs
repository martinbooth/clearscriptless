﻿namespace ClearScriptLess.Example
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using ClearScriptLess.Compiler;
    using ClearScriptLess.Compiler.Functions;

    using Microsoft.ClearScript;

    public class ImageSizesPlugin
    {
        [ScriptMember("getImageSizeHeight")]
        public static Dimension GetImageSizeHeight(FunctionParameter key)
        {
            return new Dimension("25", "px");
        }

        [ScriptMember("getImageSizeWidth")]
        public static Dimension GetImageSizeWidth(FunctionParameter key)
        {
            return new Dimension("125", "px");
        }
    }
}