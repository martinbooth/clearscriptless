﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ClearScriptLess.Example.Startup))]
namespace ClearScriptLess.Example
{
    public partial class Startup 
    {
        public void Configuration(IAppBuilder app) 
        {
            ConfigureAuth(app);
        }
    }
}
